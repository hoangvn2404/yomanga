class CreateBookmarks < ActiveRecord::Migration
  def change
    create_table :bookmarks do |t|
      t.integer :user_id
      t.integer :manga_id

      t.timestamps null: false
    end
      add_index :bookmarks, :user_id
      add_index :bookmarks, :manga_id
      add_index :bookmarks, [:user_id, :manga_id], unique: true
  end
end
