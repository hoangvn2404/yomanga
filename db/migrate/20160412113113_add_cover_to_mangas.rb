class AddCoverToMangas < ActiveRecord::Migration
  def change
    add_column :mangas, :cover, :boolean, default: false
    add_column :mangas, :coverpic, :string
  end
end
