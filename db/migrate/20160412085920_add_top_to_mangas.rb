class AddTopToMangas < ActiveRecord::Migration
  def change
    add_column :mangas, :top, :boolean, default: false
  end
end
