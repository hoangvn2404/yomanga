class AddNewToMangas < ActiveRecord::Migration
  def change
    add_column :mangas, :new, :boolean, default: false
  end
end
