class CreateMangas < ActiveRecord::Migration
  def change
    create_table :mangas do |t|
      t.string :title
      t.string :authors, array: true, default: []
      t.string :genres, array: true, default: []
      t.string :thumbnail
      t.string :status
      t.string :description
      t.string :source
      t.timestamps null: false
    end
  end
end
