class CreateChapters < ActiveRecord::Migration
  def change
    create_table :chapters do |t|
      t.string :chapter_number
      t.integer :chapter_index
      t.string :picture_urls, array: true, default: []
      t.references :manga
      t.datetime :date
      t.timestamps null: false
    end
  end
end
