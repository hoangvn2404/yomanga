class HardWorker
  include Sidekiq::Worker
  def perform_in(name, count)
    for i in 1..count do
      puts name + 'doing the' + i.to_s + 'time'
    end
  end
end
