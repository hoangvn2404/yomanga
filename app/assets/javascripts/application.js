// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap-sprockets
//= require typeahead

$(document).ready(function(){

    $('[data-toggle="tooltip"]').tooltip({
      placement : 'right',
      html: true
    });

   	$('.triStateCheckBox').on('click',function(){
   		if ($(this).hasClass('btn-default')) {
   			$(this).removeClass('btn-default');
   			$(this).addClass('btn-success');
   			$(this).children('span').addClass('glyphicon-ok')
   			$(this).children('input').attr('value','YES')
   		} else if ($(this).hasClass('btn-success')) {
   			$(this).removeClass('btn-success');
   			$(this).addClass('btn-danger');
   			$(this).children('span').removeClass('glyphicon-ok')
   			$(this).children('span').addClass('glyphicon-remove')
   			$(this).children('input').attr('value','NO')
   		} else {
   			$(this).removeClass('btn-danger');
   			$(this).addClass('btn-default');
   			$(this).children('span').removeClass('glyphicon-remove')
   			$(this).children('input').attr('value','na')
   		}

   	})

    $('#clearform').on('click',function(){
      $('.triStateCheckBox').removeClass('btn-danger');
      $('.triStateCheckBox').removeClass('btn-success');
      $('.triStateCheckBox').addClass('btn-default');
      $('.triStateCheckBox span').removeClass('glyphicon-remove');
      $('.triStateCheckBox span').removeClass('glyphicon-ok');
      $('.triStateCheckBox input').attr('value','na');
      $('.status-button label').removeClass('active');
      $('.status-button label').first().addClass('active')
    });



});


$(document).ready(function() {
  if ($('#infinite-scrolling').size() > 0) {
    $(window).scroll(function() {
      if ($('li.next_page').hasClass('disabled') == false) {
        var url = $('.pagination .next_page a').attr('href');
        if (url && $(window).scrollTop() > $(document).height() - $(window).height() - 50) {
          $('.pagination').html('<img src="loading.gif" alt="Loading..." title="Loading..." />')
          return $.getScript(url);
        }  
      }
      
    });
    // return $(window).scroll();
  }
});

