class PagesController < ApplicationController
  def home
    # HardWorker.perform_async('bob',5)

    # @feeds_mangas = Manga.all.sort_by(&:updated_at).reverse.take(12)
    @feeds_mangas = Manga.paginate(page: params[:page], per_page: 12).order('updated_at DESC')
    @top_mangas = Manga.all.where(top: true)
    @new_mangas = Manga.all.where(new: true)
    @cover_mangas = Manga.all.where(cover: true)
   respond_to do |format|
		  format.html
		  format.js
		end


  end

end
