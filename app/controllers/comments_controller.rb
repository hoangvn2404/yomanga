class CommentsController < ApplicationController


  def create
    @comment = Comment.create(commentable_id: params[:commentable_id], commentable_type: params[:commentable_type], content: params[:comment][:content], user_id: current_user.id)
    if @comment.save
      if params[:commentable_type] == 'Manga'
        @commentable = Manga.find_by(id: params[:commentable_id])
      else
        @commentable = Chapter.find_by(id: params[:commentable_id])
      end
      @comments = @commentable.comments.reverse
    else
      render :action => 'new'
    end
    # redirect_to session[:previous_url]
    respond_to do |format|
      format.html { redirect_to session[:previous_url] }
      format.js
    end
  end




end
