class MangasController < ApplicationController
  require 'will_paginate/array'
  def index
    # @mangas = Manga.all
    @mangas = Manga.paginate(page: params[:page])
    @top_mangas = Manga.all.where(top: true)
    @new_mangas = Manga.all.where(new: true)
    @genres = Manga.all.map(&:genres).join(",").split(",").uniq
  end

  def subcrible
    @mangas = current_user.mangas.paginate(:page => params[:page], :per_page => 20)
    @genres = Manga.all.map(&:genres).join(",").split(",").uniq
    render 'index'
  end

  def show
    @manga = Manga.friendly.find(params[:id])
    @chapters = @manga.chapters.all
    @comments = @manga.comments
    @commentable = @manga
    @commentable_type = 'Manga'
    @top_mangas = Manga.all.where(top: true)
    @new_mangas = Manga.all.where(new: true)
  end

  def search
    genres_yes = []
    genres_no = []
    @mangas = Manga.all
    @genres = Manga.all.map(&:genres).join(",").split(",").uniq
    @genres.each do |genre|
      g =genre.gsub(" ","_")
      if params[g] == "YES"
        genres_yes << genre
      elsif params[g] == "NO"
        genres_no << genre
      end
    end


    if params[:title]
      @mangas =[]
      Manga.all.each do |manga|
        puts manga.title
        @mangas << manga if manga.title[params[:title]]
      end
    end

    if params[:author]
      str =[]
      str << params[:author]
      @mangas = Manga.where.contains(authors: str)
    end

    if params[:genre] || genres_yes.any?
      genres_yes << params[:genre]
      genres_yes.compact!
      if genres_no.any?
        @mangas_no = Manga.where.contains(genres: genres_no)
        @mangas_yes = Manga.where.contains(genres: genres_yes)
        @mangas_excluded = Manga.all - @mangas_no
        @mangas = @mangas_yes & @mangas_excluded
      else
        @mangas = Manga.where.contains(genres: genres_yes)
      end
    end

    if (params['status'] == 'Completed') || (params['status'] == 'Ongoing')
      @mangas_status = Manga.where(status: params['status'])
      @mangas = @mangas_status & @mangas
    end

    @paginatable_mangas = @mangas
    @mangas = @paginatable_mangas.paginate(:page => params[:page], :per_page => 20)
    render 'index'
  end

end
