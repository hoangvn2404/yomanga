class ChaptersController < ApplicationController
  def show
    @manga = Manga.friendly.find(params[:manga_id])
    @chapters = @manga.chapters
    @chapter = @chapters.friendly.find(params[:id])
    chapter_index = @chapter.chapter_index
    chapter_index_next = chapter_index - 1
    chapter_index_prev = chapter_index + 1
    @next_chap = @chapters.find_by(chapter_index: chapter_index_next)
    @prev_chap = @chapters.find_by(chapter_index: chapter_index_prev)
    @comments = @chapter.comments.reverse
    @commentable = @chapter
    @commentable_type = 'Chapter'
  end
end
