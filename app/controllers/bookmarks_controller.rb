class BookmarksController < ApplicationController
  def new
    @bookmark = current_user.bookmarks.new
  end

  def create
    @manga = Manga.find_by(id: params[:manga_id])
    current_user.follow(@manga)
    # redirect_to manga
    respond_to do |format|
      format.html { redirect_to @manga }
      format.js
    end
  end

  def destroy
    # Bookmark.find_by(id: params[:id]).destroy
    @manga = Manga.find_by(id: params[:manga_id])
    current_user.unfollow(@manga)
    # redirect_to manga
    respond_to do |format|
      format.html { redirect_to @manga }
      format.js
    end

  end

  def index
    @mangas = current_user.mangas
    render mangas_path
  end
end
