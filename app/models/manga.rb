class Manga < ActiveRecord::Base
  has_many :chapters, dependent: :destroy
  # paginates_per 30
  has_many :bookmarks, dependent: :destroy
  has_many :users, through: :bookmarks
  has_many :comments, :as => :commentable
  extend FriendlyId
  friendly_id :title, use: :slugged
end
