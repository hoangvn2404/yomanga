class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :bookmarks, dependent: :destroy
  has_many :mangas, through: :bookmarks
  has_many :comments, :as => :commentable
  def follow(manga)
    bookmarks.create(manga_id: manga.id)
  end

  def unfollow(manga)
    bookmarks.find_by(manga_id: manga.id).destroy
  end

  def follow?(manga)
    mangas.include?(manga)
  end
end
