class Chapter < ActiveRecord::Base
  belongs_to :manga
  default_scope -> { order(chapter_index: :asc) }
  has_many :comments, :as => :commentable
  extend FriendlyId
  friendly_id :chapter_number, use: :slugged
end
