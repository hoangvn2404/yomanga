class Bookmark < ActiveRecord::Base
  belongs_to :user, class_name: "User"
  belongs_to :manga, class_name: "Manga"
  validates :user_id, presence: true
  validates :manga_id, presence: true
end
