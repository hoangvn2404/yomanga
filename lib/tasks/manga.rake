require 'nokogiri'
require 'open-uri'

namespace :manga do
  desc "TODO"
  task get_yomanga: :environment do
    # html_data = open('http://raws.yomanga.cor/directory').read
    manga_directory = Nokogiri::HTML(open('http://yomanga.co/reader/directory'))
    manga_urls = manga_directory.xpath("//div[@class='group']/div[@class='title']/a/@href")

    manga_urls.each_with_index do |url, index|

      manga_info = Nokogiri::HTML(open(url.value).read)
      manga_picture = manga_info.xpath("//div[@class='thumbnail']/img/@src").first.value
      manga_title = manga_info.xpath("//div[@class='large comic']/h1/text()").to_s.squish
      puts "---#{index} --- downloading: " + manga_title
      manga_authors = []
      manga_author = manga_info.xpath("//b[text()='Author']/following-sibling::text()[1]").each do |au|
        manga_authors << au.text
      end
      manga_description = manga_info.xpath("//b[text()='Synopsis']/following-sibling::text()[1]").text

      # Create new manga
      m = Manga.find_by(title: manga_title)
      if m.nil?
        m = Manga.create(source: 'yomanga.co',title: manga_title, thumbnail: manga_picture, authors: manga_authors, description: manga_description)
      end
      # Create chapters
      manga_info.xpath("//div[@class='element']/div[@class='title']/a").each_with_index do |chapter, index|
          chapter_number = chapter.children.text
          puts "---#{index} --- downloading: " + chapter_number
          if m.chapters.find_by(chapter_number: chapter_number).nil?
            chapter_url = chapter.first[1]
            chapter_date = New.datetime
            chapter_pictures = []
            page_number = 1;
            page_numbers = Nokogiri::HTML(open(chapter_url)).xpath("//div[@class='tbtitle dropdown_parent dropdown_right mmh']/div[@class='text']").first.children.text.delete(" ⤵").to_i
            for number in 1..page_numbers do
              url2 = chapter_url + "page/" + number.to_s
              chapter_picture = Nokogiri::HTML(open(url2)).xpath("//img[@class='open']/@src").first.value
              chapter_pictures << chapter_picture
            end
            m.chapters.create(chapter_index: index, chapter_number: chapter_number, picture_urls: chapter_pictures)
          end
      end
    end
  end

  desc "TODO"
  task :get_mangalife, [:id] => :environment do |t, args|
    puts "Downloading manga"
    id = args[:id].to_i
    manga_directory = Nokogiri::HTML(open('http://manga.life/directory/'))
    manga_urls = manga_directory.xpath("//p[@class='seriesList chapOnly']/a/@href")

    manga_urls.each_with_index do |u, index|
      m_index = index
      if m_index >= id
        url = "http://manga.life" + u.text.delete("..")
        manga_info = Nokogiri::HTML(open(url).read)
        manga_picture = manga_info.xpath("//div[@class='col-lg-3 col-md-3 col-sm-3 hidden-xs']/img/@src").to_s
        manga_title = manga_info.xpath("//div[@class='col-lg-8 col-md-8 col-sm-8 col-xs-12']/h1/text()").to_s
        manga_status = manga_info.xpath("//b[text()='Publishing Status: ']/following-sibling::text()").to_s
        # puts "- Manga #{index} --- " + manga_title

        manga_genres = []

        manga_info.xpath("//b[text()='Genre: ']/following-sibling::a/text()").each do |genre|
          manga_genres << genre.to_s
        end

        manga_authors = []
        manga_info.xpath("//b[text()='Author: ']/following-sibling::a/text()").each do |au|
          manga_authors << au.to_s
        end
        manga_description = manga_info.xpath("//strong[text()='Description: ']/following-sibling::div/text()").first.text
        fix_utf8(manga_description)
        # Create new manga
        m = Manga.find_by(title: manga_title)
        if m.nil?
          begin
            m = Manga.create(title: manga_title, thumbnail: manga_picture, authors: manga_authors, description: fix_utf8(manga_description), genres: manga_genres, status: manga_status, source: 'Manga.life')
          rescue => e
            binding.pry
          end

        end
        # Create chapters
        # arr=[]
        manga_info.css(".list .row").each_with_index do |chapter, index|
          # arr[index] = Thread.new {
            chapter_index = index
            chapter_url = "http://manga.life" + chapter.css(".col-md-9 a").attr('href').value.chomp('page-1')
            chapter_date = chapter.css('.col-md-3 time').attr('datetime').value
            chapter_number = chapter.css('a').children.to_s.squish

            if m.chapters.find_by(chapter_number: chapter_number).nil?

              pictures_info = Nokogiri::HTML(open(chapter_url))
              chapter_pictures_urls = []
              pictures_info.css('.imagePage img').each do |src|
                chapter_pictures_urls << src.attr('src')
              end
              m.chapters.create(chapter_number: chapter_number, picture_urls: chapter_pictures_urls, date: chapter_date, chapter_index: chapter_index)
              puts "Downloaded manga " + m_index.to_s + ': '+ m.title + ' ---------- ' + chapter_number
            else
              chapter = m.chapters.find_by(chapter_number: chapter_number)
              chapter.update_attribute(:chapter_index, chapter_index)
            end
          # }
        end
        # arr.each do |t|
        #   t.join
        # end
      end
    end
  end

  desc "TODO"
  task get_mangalife_top: :environment do
    Manga.all.each do |m|
      m.update_attribute(:top, false)
    end
    manga_directory = Nokogiri::HTML(open('http://manga.life/'))
    manga_urls = manga_directory.xpath("//div[@class='list']/a[text()='Top Manga ']/following-sibling::a/@href")

    manga_urls.each_with_index do |u, index|
      m_index = index
      if m_index >= 0
        url = "http://manga.life" + u.text.delete("..")
        manga_info = Nokogiri::HTML(open(url).read)
        manga_picture = manga_info.xpath("//div[@class='col-lg-3 col-md-3 col-sm-3 hidden-xs']/img/@src").to_s
        manga_title = manga_info.xpath("//div[@class='col-lg-8 col-md-8 col-sm-8 col-xs-12']/h1/text()").to_s
        manga_status = manga_info.xpath("//b[text()='Publishing Status: ']/following-sibling::text()").to_s
        # puts "- Manga #{index} --- " + manga_title

        manga_genres = []

        manga_info.xpath("//b[text()='Genre: ']/following-sibling::a/text()").each do |genre|
          manga_genres << genre.to_s
        end

        manga_authors = []
        manga_info.xpath("//b[text()='Author: ']/following-sibling::a/text()").each do |au|
          manga_authors << au.to_s
        end
        manga_description = manga_info.xpath("//strong[text()='Description: ']/following-sibling::div/text()").first.text
        fix_utf8(manga_description)
        # Create new manga
        m = Manga.find_by(title: manga_title)
        if m.nil?
          begin
            m = Manga.create(title: manga_title, thumbnail: manga_picture, authors: manga_authors, description: fix_utf8(manga_description), genres: manga_genres, status: manga_status, source: 'Manga.life')
          rescue => e
            binding.pry
          end
        end
        m.update_attribute(:top, true)
        # Create chapters
        # arr=[]
        manga_info.css(".list .row").each_with_index do |chapter, index|
          # arr[index] = Thread.new {
            chapter_index = index
            chapter_url = "http://manga.life" + chapter.css(".col-md-9 a").attr('href').value.chomp('page-1')
            chapter_date = chapter.css('.col-md-3 time').attr('datetime').value
            chapter_number = chapter.css('a').children.to_s.delete("\n").delete("\t")

            if m.chapters.find_by(chapter_number: chapter_number).nil?

              pictures_info = Nokogiri::HTML(open(chapter_url))
              chapter_pictures_urls = []
              pictures_info.css('.imagePage img').each do |src|
                chapter_pictures_urls << src.attr('src')
              end
              m.chapters.create(chapter_number: chapter_number, picture_urls: chapter_pictures_urls, date: chapter_date, chapter_index: chapter_index)
              puts "Downloaded manga " + m_index.to_s + ': '+ m.title + ' ---------- ' + chapter_number
            else
              chapter = m.chapters.find_by(chapter_number: chapter_number)
              chapter.update_attribute(:chapter_index, chapter_index)
            end
          # }
        end
        # arr.each do |t|
        #   t.join
        # end
      end
    end
  end

  desc "TODO"
  task get_mangalife_new: :environment do
    Manga.all.each do |m|
      m.update_attribute(:new, false)
    end
    manga_directory = Nokogiri::HTML(open('http://manga.life/'))
    manga_urls = manga_directory.xpath("//div[@class='list']/span[text()='New Manga ']/following-sibling::a/@href")

    manga_urls.each_with_index do |u, index|
      m_index = index
      if m_index >= 0
        url = "http://manga.life" + u.text.delete("..")
        manga_info = Nokogiri::HTML(open(url).read)
        manga_picture = manga_info.xpath("//div[@class='col-lg-3 col-md-3 col-sm-3 hidden-xs']/img/@src").to_s
        manga_title = manga_info.xpath("//div[@class='col-lg-8 col-md-8 col-sm-8 col-xs-12']/h1/text()").to_s
        manga_status = manga_info.xpath("//b[text()='Publishing Status: ']/following-sibling::text()").to_s
        # puts "- Manga #{index} --- " + manga_title

        manga_genres = []

        manga_info.xpath("//b[text()='Genre: ']/following-sibling::a/text()").each do |genre|
          manga_genres << genre.to_s
        end

        manga_authors = []
        manga_info.xpath("//b[text()='Author: ']/following-sibling::a/text()").each do |au|
          manga_authors << au.to_s
        end
        manga_description = manga_info.xpath("//strong[text()='Description: ']/following-sibling::div/text()").first.text
        fix_utf8(manga_description)
        # Create new manga
        m = Manga.find_by(title: manga_title)
        if m.nil?
          begin
            m = Manga.create(title: manga_title, thumbnail: manga_picture, authors: manga_authors, description: fix_utf8(manga_description), genres: manga_genres, status: manga_status, source: 'Manga.life')

          rescue => e
            binding.pry
          end

        end
        m.update_attribute(:new, true)
        # Create chapters
        # arr=[]
        manga_info.css(".list .row").each_with_index do |chapter, index|
          # arr[index] = Thread.new {
            chapter_index = index
            chapter_url = "http://manga.life" + chapter.css(".col-md-9 a").attr('href').value.chomp('page-1')
            chapter_date = chapter.css('.col-md-3 time').attr('datetime').value
            chapter_number = chapter.css('a').children.to_s.delete("\n").delete("\t")

            if m.chapters.find_by(chapter_number: chapter_number).nil?

              pictures_info = Nokogiri::HTML(open(chapter_url))
              chapter_pictures_urls = []
              pictures_info.css('.imagePage img').each do |src|
                chapter_pictures_urls << src.attr('src')
              end
              m.chapters.create(chapter_number: chapter_number, picture_urls: chapter_pictures_urls, date: chapter_date, chapter_index: chapter_index)
              puts "Downloaded manga " + m_index.to_s + ': '+ m.title + ' ---------- ' + chapter_number
            else
              chapter = m.chapters.find_by(chapter_number: chapter_number)
              chapter.update_attribute(:chapter_index, chapter_index)
            end
          # }
        end
        # arr.each do |t|
        #   t.join
        # end
      end
    end
  end

  desc "TODO"
  task get_mangalife_carousel: :environment do

    manga_directory = Nokogiri::HTML(open('http://manga.life/'))
    manga_directory.css('#myCarousel a').each do |manga_link|
      url = "http://manga.life" + manga_link.attr('href').delete("..")
      manga_info = Nokogiri::HTML(open(url).read)
      manga_title = manga_info.xpath("//div[@class='col-lg-8 col-md-8 col-sm-8 col-xs-12']/h1/text()").to_s
      coverpic = manga_link.css('img').attr('src').value
      m = Manga.find_by(title: manga_title)
      if m.nil?
        manga_picture = manga_info.xpath("//div[@class='col-lg-3 col-md-3 col-sm-3 hidden-xs']/img/@src").to_s
        manga_status = manga_info.xpath("//b[text()='Publishing Status: ']/following-sibling::text()").to_s
        manga_genres = []
        manga_info.xpath("//b[text()='Genre: ']/following-sibling::a/text()").each do |genre|
          manga_genres << genre.to_s
        end
        manga_authors = []
        manga_info.xpath("//b[text()='Author: ']/following-sibling::a/text()").each do |au|
          manga_authors << au.to_s
        end
        manga_description = manga_info.xpath("//strong[text()='Description: ']/following-sibling::div/text()").first.text
        fix_utf8(manga_description)
        m = Manga.create(title: manga_title, thumbnail: manga_picture, authors: manga_authors, description: fix_utf8(manga_description), genres: manga_genres, status: manga_status, source: 'Manga.life', cover: true, coverpic: coverpic)
      else
        m.update_attribute(:cover, true)
        m.update_attribute(:coverpic, coverpic)
      end
      manga_info.css(".list .row").each_with_index do |chapter, index|
        chapter_index = index
        chapter_url = "http://manga.life" + chapter.css(".col-md-9 a").attr('href').value.chomp('page-1')
        chapter_date = chapter.css('.col-md-3 time').attr('datetime').value
        chapter_number = chapter.css('a').children.to_s.delete("\n").delete("\t")
        if m.chapters.find_by(chapter_number: chapter_number).nil?
          pictures_info = Nokogiri::HTML(open(chapter_url))
          chapter_pictures_urls = []
          pictures_info.css('.imagePage img').each do |src|
            chapter_pictures_urls << src.attr('src')
          end
          m.chapters.create(chapter_number: chapter_number, picture_urls: chapter_pictures_urls, date: chapter_date, chapter_index: chapter_index)
          puts "Downloaded manga " + ': '+ m.title + ' ---------- ' + chapter_number
        else
          chapter = m.chapters.find_by(chapter_number: chapter_number)
          chapter.update_attribute(:chapter_index, chapter_index)
        end
      end
    end
  end

  desc "TODO"
  task update_manga_updated_date: :environment do
    @mangas = Manga.all
    @mangas.each_with_index do |manga, index|
      chap = manga.chapters.min_by(&:chapter_index)
      if chap.valid?
        manga.update_attribute(:updated_at, chap.updated_at)
        puts 'update manga No. ' + index.to_s + '----' + manga.title
      end
    end
  end

  desc "TODO"
  task update_status: :environment do
    Manga.all.each do|manga|
      status = manga.status.to_s.squish
      manga.update_attribute(:status, status)
    end
  end

  desc "TODO"
  task print: :environment do
    puts "Testing    " + Time.zone.now.to_s
  end  

end


def fix_utf8(text)
  text = text.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
end
